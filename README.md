# STM32F103C8T6运行FreeRTOS指南

本仓库提供了一个在STM32F103C8T6微控制器上运行FreeRTOS的资源文件。通过本资源，您可以简单了解FreeRTOS的使用方法和执行过程。

## 资源描述

该资源文件包含了将FreeRTOS内核文件嵌入到STM32F103C8T6中运行的示例代码。您可以通过阅读Doc文件夹下的`readme.txt`文件来了解FreeRTOS的基本使用方法和执行过程。

## 使用步骤

1. **完成函数的引脚功能初始化**  
   在开始创建任务之前，确保所有硬件引脚的功能已经正确初始化。

2. **创建任务函数**  
   参考`led.c`文件中的示例任务函数`void led0_task(void *pvParameters)`，创建您自己的任务函数。

3. **创建任务句柄**  
   在`rtos.c`文件中创建任务句柄，例如：
   ```c
   TaskHandle_t LED0Task_Handler; // 任务句柄
   ```

4. **声明任务优先级和堆栈大小**  
   在`rtos.h`文件中声明任务的优先级和堆栈大小，并定义全局任务句柄变量。

5. **创建任务**  
   在`rtos.c`文件中使用`xTaskCreate`函数创建任务，例如：
   ```c
   xTaskCreate(led0_task, "led0_task", LED0_STK_SIZE, NULL, LED0_TASK_PRIO, &LED0Task_Handler);
   ```

## 注意事项

- 请确保在创建任务之前，所有必要的硬件初始化已经完成。
- 任务的优先级和堆栈大小应根据实际需求进行调整。

## 文档

- `Doc/readme.txt`：包含FreeRTOS的基本使用方法和执行过程的详细说明。

## 贡献

欢迎提交问题和改进建议。如果您有更好的实现方法或示例代码，欢迎提交Pull Request。

## 许可证

本项目采用MIT许可证，详情请参阅`LICENSE`文件。